%% Compute invariants of lena image up to the specified order

lena = double(imread('lena.tif'));

order = 5; 

%% Appell 'U' and 'V' rotation invariants
%   A_U, A_V ... matrices of invariants

A_U = Appell_rotation_invariants(lena, order, 'U');

A_V = Appell_rotation_invariants(lena, order, 'V');

%% Zernike rotation invariants
%   Zvec    ... vector of invariants
%   Z       ... matrix of invariants
%   Zind    ... vector of indicators,
%               - ind(1, i) = i is the number of i-th invariant,
%               - ind(2, i) is order of i-th invariant,
%               - ind(3, i) is repetition and
%               - ind(4, i) = 0 for real part and 
%                 ind(4, i) = 1 for imaginary part.
%   Znormom ... vector of three numbers describing the moment normalizing
%               rotation [order, repetition, normalizing angle].

[Zvec, Z, Zind, Znormom] = zermi(lena, order);

%% Gaussian-Hermite rotation invariants
%   GHvec ... vector of invariants
%   GHpqi ... information about index and imaginarity
%             - pqi(1,ni) is 1st index, 
%             - pqi(2,ni) is 2nd index of ni-th invariant,
%             - if pqi(3,ni)==0, the ni-th invariants is based on the real
%               part of the moment GH_{pqi(1,ni),pqi(2,ni)} else on the 
%               imaginary part.

[GHvec, GHpqi, ~] = gauss_hermite(lena, order);

%% CHebyshev-Fourier rotation invariants
%   CFvec  ... vector of invariants
%   CF     ... matrix of invariants
%              A_{r, ell} = invmat(r+1, (r+ell)/2+1)
%   CFind  ... vector of indicators
%              - ind(1, i) = i is the number of i-th invariant,
%              - ind(2, i) is order of i-th invariant,
%              - ind(3, i) is repetition and
%              - ind(4, i) = 0 for real part and 
%                ind(4,i)=1 for imaginary part.
%   CFnormom ... vector of three numbers describing the moment normalizing
%              rotation [order, repetition, normalizing angle].

[CFvec, CF, CFind, CFnormom, ~] = chebyshev_fourier(lena, order);
