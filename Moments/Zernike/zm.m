function [A, rmax, tx, ty] = zm(img, order, center, radius, coef)
% Compute Zernike moments of the image.
%
% Input arguments
% ---
%   img     ... image matrix n1 x n2
%   order   ... maximum order
%   center  ... what is mapped onto the center of unit disk (default 1)
%               - 1 - center of the image
%               - 2 - centroid of the image
%   radius  ... what is mapped onto the unit circle (default 2)
%               - 1 - the most distant corner
%               - 2 - the most distant nonzero pixel
%               - 3 - the sqrt(m00)
%   coef    ... the radius mapped onto the unit circle is multiplied by
%               coef (default 1).
%               It should be set so all objects from a database were mapped
%               into the unit disk.
%
% Output arguments
% ---
% 	A       ... matrix of moments
%               A_{mn} = A(m+1,(m+n)/2+1).
%   rmax    ... radius mapped onto the unit circle
%   tx, ty  ... are coordinates mapped onto the center of the unit disk.

if nargin < 3
    center = 2;
end

if nargin < 4
    radius = 3;
end

if nargin < 5
    coef = 1;
end

[n1, n2] = size(img);

tx = (n2 + 1) / 2;
ty = (n1 + 1) / 2;

if center == 2
    m00 = sum(img(:));
    w = 1:n2;
    v = 1:n1;
    
    if m00 ~= 0
        tx = (sum(img * w')) / m00;
        ty = (sum(v * img)) / m00;
    end
    
end

rmax = sqrt(max([(1 - tx)^2 + (1 - ty)^2, (n2 - tx)^2 + (1 - ty)^2, ...
                 (n2 - tx)^2 + (n1 - ty)^2, (1 - tx)^2 + (n1 - ty)^2]));

A = zeros(order + 1, order + 1);
[y, x, v] = find(img);

if isempty(v)
    return
end

x = x - tx;
y = y - ty;

if radius == 2
    rmax = max((x.^2 + y.^2).^0.5);
elseif radius == 3
    mc = max(img(:));
    %sqrt(n2/n1+n1/n2)/sqrt(2) is correction for rectangular images
    rmax = sqrt(m00 / mc) * sqrt(n2 / n1 + n1 / n2) / sqrt(2);
end

rmax = rmax * coef;

x = x / rmax;
y = y / rmax;
r = sqrt(x.^2 + y.^2);
theta = atan2(y, x);

%Kintner method
for n = -order:order %repetition
    an = abs(n);
    rmn0 = v .* r.^an;
    vmn = rmn0 .* exp(-1i * n * theta);
    A(an + 1, (an + n) / 2 + 1) = (an + 1) / pi * sum(vmn(:));
    
    if order - an >= 2
        rmn2 = v .* ((an + 2) * r.^(an + 2) - (an + 1) * r.^an);
        vmn = rmn2 .* exp(-1i * n * theta);
        A(an + 3, (an + 2 + n) / 2 + 1) = (an + 3) / pi * sum(vmn(:));
    end
    
    for m = an + 4:2:order %order
        k1 = (m + n) * (m - n) * (m - 2) / 2;
        k2 = 2 * m * (m - 1) * (m - 2);
        k3 = -n^2 * (m - 1) - m * (m - 1) * (m - 2);
        k4 = -m * (m + n - 2) * (m - n - 2) / 2;
        rmn4 = ((k2 * r.^2 + k3 * ones(size(v))) .* rmn2 + k4 * rmn0) / k1;
        vmn = rmn4 .* exp(-1i * n * theta);
        A(m + 1, (m + n) / 2 + 1) = (m + 1) / pi * sum(vmn(:));
        rmn0 = rmn2;
        rmn2 = rmn4;
    end
    
end

A = A / rmax^2;
