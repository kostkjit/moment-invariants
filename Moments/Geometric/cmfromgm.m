function c = cmfromgm(order, gm)
% Compute complex moments c_{pq} from geometric moments gm
% up to the specified order.
%
% Input arguments
% ---
%   order ... maximum order of moments
%   gm    ... matrix of geometric moments (order +1) x (order +1)
%
% Output arguments
% ---
%   c ... matrix of complex moments

c = zeros(order + 1);
for p = 0 : order
    for q = 0 : order - p
        for k = 0 : p
            pk = nchoosek(p, k);
            for j = 0 : q
                qj = nchoosek(q, j);
                c(p+1, q+1) = c(p+1, q+1) + ...
                    pk * qj * (-1)^(q - j) * 1i^(p + q - k - j) *...
                    gm(k + j + 1, p + q - k - j + 1);
            end
        end
    end
end

