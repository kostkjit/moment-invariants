function [cg, tx, ty] = cm(img, r, t)
% Compute central moments cg up to r-th order of the image img,
% \mu_{pq} = cg(p+1,q+1)
%
% Input arguments
% ---
%   img ... image matrix n1 x n2
%   r   ... maximum order of moments
%   t   ... transform image to unit square (default 0)
%
% Output arguments
% ---
%   cg       ... matrix of central moments
%   tx, ty  ... coordinates of the centroid

if nargin < 3
    t = 0;
end

[n1, n2] = size(img);

m00 = sum(sum(img));

if t == 1
    w = linspace(0, 1, n2);
    v = linspace(0, 1, n1);
else
    w = 1:n2;
    v = 1:n1;
end

if m00 ~= 0
    tx = (sum(img * w')) / m00;
    ty = (sum(v * img)) / m00;
else
    if t == 1
        tx = 0.5;
        ty = 0.5;
    else
        tx = (n2 + 1) / 2;
        ty = (n1 + 1) / 2;
    end
end

a = w - tx;
c = v - ty;

if t == 1 
    a = a / max(abs(a(:)));
    c = c / max(abs(c(:)));
end

A = repmat(a, r + 1, 1).^repmat((0:r)', 1, n2);
C = repmat(c, r + 1, 1).^repmat((0:r)', 1, n1);
cg = A * img' * C';

if r > 0
    cg(1, 2) = 0;
    cg(2, 1) = 0;
end
