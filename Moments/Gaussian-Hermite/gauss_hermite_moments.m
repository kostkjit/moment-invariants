function [ghm, aspect] = gauss_hermite_moments(img, r, sigma, normcoef,...
    center, typemap, mapcoef)
% Compute values of Gaussian-Hermite moments.
%
% Input arguments
% ---
%   img      ... input image
%   r        ... maximum order of invariants
%   sigma    ... standard deviation parameter of the GH polynomials
%                (default 0.3)
%   normcoef ... normalizing coefficient (default 0.5)
%               - 1 - ghm_{p0} and ghm_{0q} are normalized precisely,
%               - 0 - ghm_{p/2,p/2} are normalized precisely
%   center   ... what is mapped onto the center of square (default 1)
%               - 1 - center of the image
%               - 2 - centroid of the image
%   typemap  ... type of mapping (default 1)
%               - 1 - mapping according to the most distant non-zero pixel
%               - 2 - mapping according to m00
%               - 3 - mapping according to the size of the image
%               - 4 - mapping according to fixed size of images in mapcoef
%   mapcoef  ... mapping is multipled by mapcoef
%
% Output arguments
% ---
%   ghm     ... matrix of Gaussian Hermite moments
%   aspect  ... ?

if nargin < 3
    % range of input values of the Gaussian-Hermite polynomial is from
    % -1/sigma to 1/sigma
    sigma = 0.3;
end

if nargin < 4
    normcoef = 0.5;
end

if nargin < 5
    center = 1;
end

if nargin < 6
    typemap = 1;
end

if nargin < 7
    mapcoef = 1;
end

normcoef = min(max(normcoef, 0), 1); % normcoef in [0, 1]

[n1, n2] = size(img);
m00 = sum(img(:));

if center == 2 && m00 ~= 0
    w = 1:n2;
    v = 1:n1;
    tx = (sum(img * w')) / m00;
    ty = (sum(v * img)) / m00;
else
    tx = (n2 + 1) / 2;
    ty = (n1 + 1) / 2;
end

tx = round(tx);
ty = round(ty);

[y, x] = find(img);
if isempty(x) || isempty (y)
    x = [1; 1; n2; n2];
    y = [1; n1; n1; 1];
end

lnp = length(x); % number of nonzero pixels
mxxy1 = max(sqrt((x - tx .* ones(lnp, 1)).^2 ...
               + (y - ty .* ones(lnp, 1)).^2));
mxxy2 = sqrt(m00) / sqrt(pi / 2);
aspect = mxxy1 / mxxy2;

if typemap == 1
    mxxy = mxxy1;
elseif typemap == 2
    mxxy = mxxy2;
elseif typemap == 3
    xv = [1; 1; n2; n2];
    yv = [1; n1; n1; 1];
    lnp = length(xv);
    mxxy = max(sqrt((xv - tx .* ones(lnp, 1)).^2 ...
                  + (yv - ty .* ones(lnp, 1)).^2));
else
    mxxy = 1;
end

mxxy = ceil(mxxy * mapcoef);
mxxy1 = ceil(mxxy1);
xy = (-mxxy1:mxxy1) / (sigma * mxxy);

ghvalues = zeros(r + 1, 2 * mxxy1 + 1);
ghvalues(1, :) = exp(-xy.^2/2);
ghvalues(2, :) = 2 * xy .* exp(-xy.^2/2);

for n = 2:r
    ghvalues(n + 1, :) = 2 * xy .* ghvalues(n, :)...
                         - (2 * n - 2) * ghvalues(n - 1, :);
end

% Gaussian - Hermite moments
ghm = zeros(r + 1, r + 1);

xi = (1:n2) - tx + mxxy1 + 1;
xi(xi < 1) = 1;
xi(xi > 2 * mxxy1 + 1) = 2 * mxxy1 + 1;

yi = (1:n1) - ty + mxxy1 + 1;
yi(yi < 1) = 1;
yi(yi > 2 * mxxy1 + 1) = 2 * mxxy1 + 1;

for p = 0:r
    
    for q = 0:r - p
        xgh = ghvalues(p + 1, xi);
        ygh = ghvalues(q + 1, yi);
        ghm(p + 1, q + 1) = ygh * img * xgh' / mxxy^2;
        ghm(p + 1, q + 1) = ghm(p + 1, q + 1) * (2^(p + q)...
            * gamma(p + q + 1)^normcoef ...
            * gamma((p + q) / 2 + 1)^(2 * (1 - normcoef)) * pi)^(-0.5);
    end
    
end
