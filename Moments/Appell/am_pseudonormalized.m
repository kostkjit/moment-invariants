function [AM, AP, rmax, tx, ty, x, y] = am_pseudonormalized(img, r,...
                                        poly, center, radius, coef)
% Compute Appell moments of the image img with normalization preserving 
% rotation invariance.
%
% Input arguments
% ---
%   img     ... image matrix
%   r       ... maximum order of moments
%   poly    ... type of polynomials
%               'U' | 'V'
%   center  ... what is mapped onto the center of unit disk (default 1)
%              - 1 - center of the image 
%              - 2 - centroid of the image
%   radius  ... what is mapped onto the unit circle (default 2)
%               - 1 - the most distant corner
%               - 2 - the most distant nonzero pixel 
%               - 3 - the sqrt(m00)
%   coef    ... radius mapped onto the unit circle is multiplied by  
%               coef (default 1)
%               It should be set so all objects from a database were mapped
%               into the unit disk.
%
% Output arguments
% ---
%   AM      ... normalized Appell moments
%   AP      ... Appell polynomials
%   rmax    ... radius mapped onto the unit circle
%   tx, ty  ... coordinates mapped onto the center of the unit disk

if nargin < 4
    center = 1;       % centroid is not stable
end
if nargin < 5
    radius = 2;
end
if nargin < 6
    coef = 1;
end


switch poly
    case 'U'
       [AM, AP, rmax, tx, ty, x, y] = am_U_pseudonormalized(img, r,...
                                    center, radius, coef);
    case 'V'
       [AM, AP, rmax, tx, ty, x, y] = am_V_pseudonormalized(img, r,...
                                    center, radius, coef);
end