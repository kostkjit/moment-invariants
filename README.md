# Moment invariants

By downloading the code you accept the following terms:

- I will acknowledge the work of the authors of the tools in all my
publications, which will utilize results, ideas or parts of the code
in some way, and I will send the preliminary or final publications to
the authors.

Matlab codes for computation of moment invariants. See Examples.m to get started.
More detailed explanation can be found in the related wiki:
https://gitlab.com/kostkjit/moment-invariants/-/wikis/home 
