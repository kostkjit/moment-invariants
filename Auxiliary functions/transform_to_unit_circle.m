function [x, y, v, max_radius, tx, ty] = transform_to_unit_circle(img, ...
                                         center, radius, coef)
% Transform image to unit circle
%
% Input arguments
% ---
%   img     ... image to be transformed
%   center  ... what is mapped onto the center of unit disk (default 1)
%               - 1 - center of the image
%               - 2 - centroid of the image
%   radius  ... what is mapped onto the unit circle (default 2)
%               - 1 - the most distant corner (suitable for reconstruction)
%               - 2 - the most distant nonzero pixel
%               - 3 - the sqrt(m00)
%   coef    ... the radius mapped onto the unit circle is multiplied by
%               coef (default 1)
%               It should be set so all objects from a database were mapped
%               into the unit disk.
%
% Output arguments
% ---
%   x, y        ... transformed coordinates
%   v(i)        ... pixel value in (x(i), y(i))
%   max_radius  ... radius mapped onto the unit circle
%   tx, ty      ... coordinates mapped onto the center of the unit disk

if nargin < 2
    center = 1;
end

if nargin < 3
    radius = 2;
end

if nargin < 4
    coef = 1;
end

[n1, n2] = size(img);

tx = (n2 + 1) / 2;
ty = (n1 + 1) / 2;
m00 = sum(img(:));

if center == 2
    w = 1:n2;
    v = 1:n1;
    
    if m00 ~= 0
        tx = (sum(img * w')) / m00;
        ty = (sum(v * img)) / m00;
    end
    
end

max_radius = sqrt(max([(1 - tx)^2 + (1 - ty)^2,...
                       (n2 - tx)^2 + (1 - ty)^2,...
                       (n2 - tx)^2 + (n1 - ty)^2,...
                       (1 - tx)^2 + (n1 - ty)^2]));
[y, x, v] = find(img);

if isempty(v)
    return
end

x = x - tx; % moving image center to zero
y = y - ty;

if radius == 2
    max_radius = max((x.^2 + y.^2).^0.5);
elseif radius == 3
    mc = max(img(:));
    %sqrt(n2/n1+n1/n2)/sqrt(2) is correction for rectangular images
    max_radius = sqrt(m00 / mc) * sqrt(n2 / n1 + n1 / n2) / sqrt(2);
end

max_radius = max_radius * coef;

x = x / max_radius;
y = y / max_radius;

end
