function [phi, phivec, ift] = RotInvs(CM, order, norm_type, scale_norm,...
                                      thres)
% Compute the classic rotation inavriants from complex moments
%
% Input arguments
% ---
%   CM          ... matrix of complex moments
%   order       ... maximum order of moments
%   norm_type   ... type of normalization to rotation (default 1)
%                   - 1 - subtracting the respective multiple of phase c_12
%                   - 2 - multiplication by the respective power of c_12
%   scale_norm  ... normalization to scaling (default 0)
%                   - 0 - no normalization
%                   - 1 - normalization to scaling is used
%   thres       ... threshold for absolute value of normalization moment
%                   (default 1e-3)
%
% Output arguments
% ---
%   phi     ... matrix of rotation invariants
%   phivec  ... vector of (non-trivial) rotation invariants
%   ift     ... vector of information [q0, p0, abs(CM(q0 + 1, p0 + 1))]
    
if nargin < 2
    order = min(size(CM)) - 1;
end

if nargin < 3
    norm_type = 1;
end

if nargin < 4
    scale_norm = 0;
end

if nargin < 5
    thres = 1e-3;
end

[q0, p0, id, flag] = choose_pq(CM, order, thres);

[Q, P] = meshgrid(1:order + 1);

if flag == 0
    
    if norm_type == 2
        A = CM(q0 + 1, p0 + 1).^((P - Q)/id);
    elseif norm_type == 1
        Theta = angle(CM) * (id - 1)...
              + angle(CM(q0 + 1, p0 + 1)) .* (P - Q);
        A = exp(1i * Theta);
    end
    
else
    A = eye(order + 1);
end

N = P >= Q;
phi = CM .* A .* N;

if CM(1, 1) ~= 0 && scale_norm == 1
    phi = phi ./ CM(1, 1).^((P + Q) / 2);
end

phivec = phi(P >= Q & P + Q <= order + 2 & P + Q ~= 3);

if flag == 0
    ift = [q0, p0, abs(CM(q0 + 1, p0 + 1))];
else
    ift = [q0, p0, 0];
end

end



function [p0, q0, id, flag] = choose_pq(M, r, thres)
% Find significant moment for normalization

flag = 1;
p0 = -1;
q0 = -1;
id = 0; %index difference

while id < r && flag == 1
    id = id + 1;
    p = 0; %first index
    
    if id == 1
        p = 1;
    end
    
    while p <= floor((r - id) / 2) && flag == 1
        q = p + id;
        
        if abs(M(p + 1, q + 1)) > thres
            flag = 0;
            p0 = p;
            q0 = q;
        end
        
        p = p + 1;
    end
    
end
end
