function [invts, pqi, aspect] = gauss_hermite(img, r, sigma, normcoef,...
                                typemap, mapcoef, center)
% Compute values of translation and rotation invariants based on
% Gaussian-Hermite moments.
%
% Input arguments
% ---
%   img      ... input image
%   r        ... maximum order of invariants
%   sigma    ... standard deviation parameter of the GH polynomials
%                (default 0.3)
%   normcoef ... normalizing coefficient (default 0.5)
%               - 1 - ghm_{p0} and ghm_{0q} are normalized precisely,
%               - 0 - ghm_{p/2,p/2} are normalized precisely
%   typemap  ... type of mapping.
%               - 1 - mapping according to the most distant non-zero pixel
%               - 2 - mapping according to m00
%               - 3 - mapping according to the size of the image
%               - 4 - mapping according to fixed size of images in mapcoef
%   mapcoef  ... mapping is multipled by mapcoef
%   center   ... what is mapped onto the center of square (default 1)
%               - 1 - center of the image
%               - 2 - centroid of the image
%
% Output arguments
% ---
%   invts ... vector of invariants
%   pqi   ... information about index and imaginarity
%             - pqi(1,ni) is 1st index, 
%             - pqi(2,ni) is 2nd index of ni-th invariant,
%             - if pqi(3,ni)==0, the ni-th invariants is based on the real
%               part of the moment GH_{pqi(1,ni),pqi(2,ni)} else on the 
%               imaginary part.
%   aspect ... ?

if nargin < 3
    %    sigma=0.9018*(1.3218*mxr+12.734)^(-0.521);
    sigma = 0.3;
end

if nargin < 4
    normcoef = 0.5;
end

if nargin < 5
    typemap = 1;
end

if nargin < 6
    mapcoef = 1;
end

if nargin < 7
    center = 2;
end
% range of input values of the Gaussian - Hermite polynomial is from
%-1/sigma to 1/sigma

[ghm, aspect] = gauss_hermite_moments(img, r, sigma, normcoef, center,...
                typemap, mapcoef);

c = cmfromgm(r, ghm); %conversion of the central moments to complex

% rotation invariants
if r == 2
    p0 = 0;
    q0 = 2;
else
    p0 = 1;
    q0 = 2;
end
id = q0 - p0; % index difference
ni = 1; % sequential number of the invariant

for r1 = max(2, id):id:r
    
    for p = round(r1 / 2):r1
        q = r1 - p;
        
        if mod(p - q, id) == 0
            invts(ni) = real(c(p + 1, q + 1)...
                        * c(p0 + 1, q0 + 1)^((p - q) / id));
            pwi(ni) = 1 + (p - q) / id;
            pqi(:, ni) = [p; q; 0];
            ni = ni + 1;
            
            if p > q && (p ~= q0 || q ~= p0)
                invts(ni) = real(c(p + 1, q + 1) * c(q + 1, p + 1));
                pwi(ni) = 2;
                pqi(:, ni) = [p; q; 1];
                ni = ni + 1;
            end
            
        end
        
    end
    
end

% magnitude normalization to degree
invts = sign(invts) .* abs(invts).^(1 ./ pwi);
