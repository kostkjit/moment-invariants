function [invts, invmat, ind, normom] = zermi(img, r, thres, typecomp,...
                                        typeinv, center, radius, coef)
% Compute rotation Zernike invariants of the image.
%
% Input arguments
% ---
%   img      ... image matrix
%   r        ... maximum order of moments (default 3)
%   thres    ... threshold of non-zero moments (default 1e-3)
%   typecomp ... method of computation (default 0)
%               - 0 - moments are computed directly by Kintner method 
%               - 1 - they are computed from geometric moments
%   typeinv  ... type of rotation invariance (default 0)
%               - 0 - rotation normalization is used
%               - 1 - phase cancellation by multiplication is used.
%   center  ... what is mapped onto the center of unit disk (default 1)
%               - 1 - center of the image
%               - 2 - centroid of the image
%   radius  ... what is mapped onto the unit circle (default 1)
%               - 1 - the most distant corner
%               - 2 - the most distant nonzero pixel
%               - 3 - the sqrt(m00)
%   coef    ... the radius mapped onto the unit circle is multiplied by
%               coef (default 1).
%               It should be set so all objects from a database were mapped
%               into the unit disk.
%
% Output arguments
% ---
%   invts   ... vector of invariants
%   invmat  ... matrix of invariants
%               A_{r, ell} = invmat(r+1, (r+ell)/2+1);
%   ind     ... vector of indicators,
%               - ind(1, i) = i is the number of i-th invariant,
%               - ind(2, i) is order of i-th invariant,
%               - ind(3, i) is repetition and
%               - ind(4, i) = 0 for real part and 
%                 ind(4, i) = 1 for imaginary part.
%   normom 	... vector of three numbers describing the moment normalizing
%               rotation [order, repetition, normalizing angle].

if nargin < 2
    r = 3;
end

if nargin < 3
    thres = 1e-3;
end

if nargin < 4
    typecomp = 0;
end

if nargin < 5
    typeinv = 0;
end

if nargin < 6
    center = 1;
end

if nargin < 7
    radius = 1;
end

if nargin < 8
    coef = 1;
end

img = double(img);

if size(img, 3) > 1
    n3 = size(img, 3);
    img = sum(img, 3) / n3;
end

[x1, y1] = find(img);
szx = size(x1);
szy = size(y1);

if (szx(1) > 0 && szy(1) > 0)
    mn1 = min(x1);
    mx1 = max(x1);
    mn2 = min(y1);
    mx2 = max(y1);
    img = img(mn1:mx1, mn2:mx2);
end

if typecomp == 0
    img = img / max(img(:)); %normalization of moments to contrast
    am = zm(img, r, center, radius, coef); %computation of Zernike moments
else
    am = Zernikefromgm(img, r);
end

[am, mt, nt, theta] = normalize_to_rotation(am, r, thres, typeinv);

% invariants
pinv = 1;

for k1 = 2:r
    
    for k2 = k1:-2:0
        invts(pinv) = real(am(k1 + 1, (k1 + k2) / 2 + 1));
        ind(:, pinv) = [pinv; k1; k2; 0];
        pinv = pinv + 1;
        
        if k2 ~= 0 && (k1 ~= 3 || k2 ~= 1)
            invts(pinv) = imag(am(k1 + 1, (k1 + k2) / 2 + 1));
            ind(:, pinv) = [pinv; k1; k2; 1];
            pinv = pinv + 1;
        end
        
    end
    
end

invmat = am;
normom = [mt, nt, theta];
end



function am = Zernikefromgm(img, r)
    [qm, pm] = meshgrid(0:r);

    %computation of central moments
    gm = cm(img, r);

    %normalization of moments to scaling
    gm00 = gm(1, 1);
    gm = gm ./ (gm00.^((pm + qm + 2) / 2));

    %conversion of moments to Zernike ones
    am = zmgm(gm, r);
end



function [am, mt, nt, theta] = normalize_to_rotation(am, r, thres, typeinv)

mt = 0;
nt = 0;
theta = 0;
flag = 0;

for k1 = 1:r
    
    for k2 = k1:2:r
        
        if k2 > 1
            
            if abs(am(k2 + 1, (k2 + k1) / 2 + 1)) > thres
                %normalizing moment found
                theta = angle(am(k2 + 1, (k2 + k1) / 2 + 1)) / k1;
                flag = 1;
                mt = k2;
                nt = k1;
            end
            
        end
        
        if flag
            break
        end
        
    end
    
    if flag
        break
    end
    
end

if flag
    
    for k1 = 2:r
        
        for k2 = -k1:2:k1
            
            if typeinv == 0
                am(k1 + 1, (k1 + k2) / 2 + 1) = ...
                    am(k1 + 1, (k1 + k2) / 2 + 1) * exp(-1i * k2 * theta);
            else
                
                if k1 ~= mt || k2 ~= nt
                    am(k1 + 1, (k1 + k2) / 2 + 1) = ...
                        am(k1 + 1, (k1 + k2) / 2 + 1) / am(mt + 1,...
                          (mt + nt) / 2 + 1)^(k2 / nt);
                end
                
            end
            
        end
        
    end
    
    if typeinv ~= 0
        am(mt + 1, (mt + nt) / 2 + 1) = abs(am(mt + 1, (mt + nt) / 2 + 1));
    end
    
end

end