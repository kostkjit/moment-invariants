function [invts, invmat, ind, normom, idcoef] = chebyshev_fourier(img,...
    r, kind, thres, center, radius, coef)
% Compute rotation Chebyshev-Fourier invariants of the image.
%
% Input arguments
% ---
%   img     ... image matrix
%   r       ... maximum order of the moments (default 3),
%   kind 	... kind of the Chebyshev polynomial
%               1 (default) | 2
%   thres 	... threshold of non-zero moments (default 1e-3)
%   center  ... what is mapped onto the center of unit disk (default 1)
%               - 1 - center of the image
%               - 2 - centroid of the image
%   radius  ... what is mapped onto the unit circle (default 1)
%               - 1 - the most distant corner
%               - 2 - the most distant nonzero pixel
%               - 3 - the sqrt(m00)
%   coef    ... the radius mapped onto the unit circle is multiplied by
%               coef (default 1).
%               It should be set so all objects from a database were mapped
%               into the unit disk.
%
% Output arguments
% ---
%   invts  ... vector of invariants
%   invmat ... matrix of invariants
%              A_{r, ell} = invmat(r+1, (r+ell)/2+1)
%   ind    ... vector of indicators
%              - ind(1, i) = i is the number of i-th invariant,
%              - ind(2, i) is order of i-th invariant,
%              - ind(3, i) is repetition and
%              - ind(4, i) = 0 for real part and 
%                ind(4,i)=1 for imaginary part.
%   normom ... vector of three numbers describing the moment normalizing
%              rotation [order, repetition, normalizing angle].
%   idcoef ... ?

if nargin < 2
    r = 3;
end

if nargin < 3
    kind = 1;
end

if nargin < 4
    thres = 1e-3;
end

if nargin < 5
    center = 1;
end

if nargin < 6
    radius = 1;
end

if nargin < 7
    coef = 1;
end

[x1, y1] = find(img);
szx = size(x1);
szy = size(y1);

if (szx(1) > 0 && szy(1) > 0)
    mn1 = min(x1);
    mx1 = max(x1);
    mn2 = min(y1);
    mx2 = max(y1);
    img = img(mn1:mx1, mn2:mx2);
end

%normalization of moments to contrast
img = img / max(img(:));

%computation of Chebyshev-Fourier moments
[CFM, ~, ~, ~, rma] = chfm(img, r, kind, center, radius, coef);
idcoef = rma(2) / rma(3);

[CFM, mt, nt, theta] = normalize_to_rotation(CFM, r, thres);

% invariants
pinv = 1;

for k1 = 2:r
    
    for k2 = k1:-2:0
        invts(pinv) = real(CFM(k1 + 1, (k1 + k2) / 2 + 1));
        ind(:, pinv) = [pinv; k1; k2; 0];
        pinv = pinv + 1;
        
        if k2 ~= 0 && (k1 ~= 3 || k2 ~= 1)
            invts(pinv) = imag(CFM(k1 + 1, (k1 + k2) / 2 + 1));
            ind(:, pinv) = [pinv; k1; k2; 1];
            pinv = pinv + 1;
        end
        
    end
    
end

invmat = CFM;
normom = [mt, nt, theta];
end


function [M, mt, nt, theta] = normalize_to_rotation(M, r, thres)

    mt = 0;
    nt = 0;
    theta = 0;
    flag = 0; 

    for k1 = 1:r

        for k2 = k1:2:r

            if k2 > 1

                if abs(M(k2 + 1, (k2 + k1) / 2 + 1)) > thres
                    % normalizing moment found
                    theta = angle(M(k2 + 1, (k2 + k1) / 2 + 1)) / k1; 
                    flag = 1;
                    mt = k2;
                    nt = k1;
                end

            end

            if flag
                break
            end

        end

        if flag
            break
        end

    end

    if flag

        for k1 = 2:r

            for k2 = -k1:2:k1
                M(k1 + 1, (k1 + k2) / 2 + 1) = ...
                    M(k1 + 1, (k1 + k2) / 2 + 1) * exp(-1i * k2 * theta);
            end

        end

    end

end
