function A = Appell_rotation_invariants(img, r, poly, norm_type, center,...
    radius, coef, thres)
% Compute rotation invariants from Appell moments of the image img with
% normalization preserving rotation invariance.
%
% Input arguments
% ---
%   img       ... image matrix
%   r         ... maximum order of invariants
%   poly      ... type of polynomials
%                 'U' | 'V'
%   norm_type ... type of normalization to rotation (default 2)
%               - 1 - subtracting the respective multiple of phase c_12
%               - 2 - multiplication by the respective power of c_12
%   center    ... what is mapped onto the center of unit disk (default 1)
%               - 1 - center of the image 
%               - 2 - centroid of the image
%   radius    ... what is mapped onto the unit circle (default 2)
%               - 1 - the most distant corner
%               - 2 - the most distant nonzero pixel 
%               - 3 - the sqrt(m00)
%   coef      ... radius mapped onto the unit circle is multiplied by  
%                 coef (default 1)
%                 It should be set so all objects from a database were
%                 mapped into the unit disk.
%   thres     ... threshold for absolute value of normalization moment
%                 (default 1e-3)
%
% Output arguments
% ---
%   A ... matrix of rotation invariants
%
% Example
% ---
%   lena = double(imread('lena.tif'));
%   A = Appell_rotation_invariants(lena, 5, 'U')

if nargin < 4
    norm_type = 2;
end

if nargin < 5
    center = 1;
end

if nargin < 6
    radius = 2;
end

if nargin < 7
    coef = 1;
end

if nargin < 8
    thres = 1e-3;
end

AM = am_pseudonormalized(img, r, poly, center, radius, coef); 
CM = cmfromgm(r, AM);
A = RotInvs(CM, r, norm_type, 0, thres);

end